package com.coderli.shurnim.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

/**
 * 应用安全相关配置
 * 
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 */
@Configuration
@ImportResource(value = { "/WEB-INF/spring/security.xml" })
public class AppSecurityConfig {

	@Bean
	public FilterSecurityInterceptor springSecurityFilterChain() {
		return new FilterSecurityInterceptor();
	} 
}
