package com.coderli.shurnim.auth.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.coderli.shurnim.auth.model.User;

/**
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 * @date 2012-10-31 上午11:10:56
 */
public class AuthUserDetails implements UserDetails {

	private static final long serialVersionUID = -7704769860281561117L;

	private User user;

	public AuthUserDetails(User user) {
		this.user = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<AuthGrantedAuthority> authList = new ArrayList<AuthGrantedAuthority>();
		authList.add(new AuthGrantedAuthority());
		return authList;
	}

	@Override
	public String getPassword() {
		return user == null ? null : user.getPassword();
	}

	@Override
	public String getUsername() {
		return user == null ? null : user.getName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return user == null ? false : user.isEnabled();
	}

}
