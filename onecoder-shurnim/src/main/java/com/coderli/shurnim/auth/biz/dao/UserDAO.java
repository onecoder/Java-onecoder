package com.coderli.shurnim.auth.biz.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coderli.shurnim.auth.model.User;
import com.coderli.shurnim.common.GenericHibernateDAO;

/**
 * 用户管理DAO类
 * 
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 */
@Repository
public class UserDAO extends GenericHibernateDAO<User> {

	@Autowired
	protected UserDAO(SessionFactory sessionFactory) {
		super(User.class, sessionFactory);
	}

	/**
	 * 根据用户名查询用户信息
	 * 
	 * @param userName
	 * @return
	 * @author lihzh
	 * @date 2012-10-23 下午3:26:08
	 */
	public User getUserByName(String userName) {
		return findUniqueEntityByProp("name", userName);
	}
}
