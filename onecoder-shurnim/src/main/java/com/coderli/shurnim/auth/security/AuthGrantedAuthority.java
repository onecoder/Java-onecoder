package com.coderli.shurnim.auth.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author lihzh
 * @date 2012-10-31 下午1:27:14
 */
public class AuthGrantedAuthority implements GrantedAuthority {

	private static final long serialVersionUID = 1300794687577661026L;

	@Override
	public String getAuthority() {
		return "ROLE_USER";
	}

}
