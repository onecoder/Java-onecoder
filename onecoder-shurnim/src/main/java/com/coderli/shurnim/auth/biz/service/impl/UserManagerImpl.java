package com.coderli.shurnim.auth.biz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.coderli.shurnim.auth.biz.dao.UserDAO;
import com.coderli.shurnim.auth.biz.service.UserManager;
import com.coderli.shurnim.auth.model.User;

/**
 * 用户管理业务接口实现类
 * 
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 */
@Service
@Transactional
public class UserManagerImpl implements UserManager {

	private UserDAO _userDAO;

	@Autowired
	public UserManagerImpl(UserDAO userDAO) {
		this._userDAO = userDAO;
	}

	@Transactional(readOnly = true)
	@Override
	public User getUserByName(String name) {
		return _userDAO.getUserByName(name);
	}

}
