package com.coderli.shurnim.auth.biz;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import com.coderli.shurnim.auth.biz.dao.UserDAO;

/**
 * 用户操作Action类
 * 
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 */
@Controller
public class UserAction {

	private static final Logger _log = LoggerFactory
			.getLogger(UserAction.class);
	@Autowired
	private UserDAO userDAO;

	@RequestMapping("/user.do")
	@Transactional
	public void test(HttpServletResponse response) throws IOException {
		_log.info("Hi, u guy");
		userDAO.getAll();
		response.getWriter().write("Hi, u guy.");
	}
}
