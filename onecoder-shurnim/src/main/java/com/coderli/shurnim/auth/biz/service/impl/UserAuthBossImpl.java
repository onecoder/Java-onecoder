package com.coderli.shurnim.auth.biz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.coderli.shurnim.auth.biz.service.UserManager;
import com.coderli.shurnim.auth.model.User;
import com.coderli.shurnim.auth.security.AuthUserDetails;

/**
 * Spring Security 用户认证管理接口实现类
 * 
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 */
@Service("userAuthBoss")
@Transactional
public class UserAuthBossImpl implements UserDetailsService {
	
	private UserManager _userManager;
	
	@Autowired
	public UserAuthBossImpl(UserManager userManager) {
		this._userManager = userManager;
	}

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		User user = _userManager.getUserByName(username);
		UserDetails userDetails = new AuthUserDetails(user);
		return userDetails;
	}

}
