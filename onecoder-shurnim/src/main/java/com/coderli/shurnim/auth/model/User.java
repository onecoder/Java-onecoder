package com.coderli.shurnim.auth.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 用户模型
 * 
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 */
@Entity
@Table(name="snm_user")
public class User implements Serializable {

	private static final long serialVersionUID = -6925982814013703984L;

	@Id
	private int id;
	@Column(length = 20, nullable = false)
	private String name;
	@Column(length = 32, nullable = false)
	private String password;
	@Column(length = 64, nullable = false)
	private String email;
	@Column(name = "ctime", length = 20, nullable = false)
	private long createTime;
	@Column(nullable = false)
	private boolean enabled;
	@Column(name = "last_login_time")
	private long lastLoginTime;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public long getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(long lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

}
