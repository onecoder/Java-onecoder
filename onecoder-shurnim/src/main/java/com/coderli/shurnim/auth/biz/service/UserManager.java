package com.coderli.shurnim.auth.biz.service;

import com.coderli.shurnim.auth.model.User;

/**
 * 用户管理业务接口
 * 
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 */
public interface UserManager {

	/**
	 * 根据用户名获取用户
	 * 
	 * @param name
	 *            用户名
	 * @return 用户对象
	 * @author lihzh
	 * @alia OneCoder
	 * @date 2012-10-23 下午3:04:42
	 */
	User getUserByName(String name);

}
