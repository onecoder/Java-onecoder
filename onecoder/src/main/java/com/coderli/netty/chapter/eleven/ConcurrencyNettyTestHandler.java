package com.coderli.netty.chapter.eleven;


import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import com.coderli.netty.chapter.common.model.Command;

/**
 * @author lihzh
 * @date 2012-7-30 下午3:14:29
 */
public class ConcurrencyNettyTestHandler extends SimpleChannelHandler {

	private static int count = 0;

	/**
	 * 当接受到消息的时候触发
	 */
	@Override
	public void channelConnected(ChannelHandlerContext ctx,
			final ChannelStateEvent e) throws Exception {
		for (int i = 0; i < 100; i++) {
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					sendObject(e.getChannel());
				}
			});
			System.out.println("Thread count: " + i);
			t.start();
		}

	}

	/**
	 * 发送Object
	 * 
	 * @param channel
	 * @author lihzh
	 * @alia OneCoder
	 */
	private void sendObject(Channel channel) {
//		for (;;) {
			count++;
			Command command = new Command();
			command.setActionName("Hello action.");
			// Command commandOne = new Command();
			// commandOne.setActionName("Hello action. One");
			// Command command2 = new Command();
			// command2.setActionName("Hello action. Two");
			// channel.write(command2);
			channel.write(command);
			System.out.println("Write: " + count);
			// channel.write(commandOne);
//		}
	}
}
