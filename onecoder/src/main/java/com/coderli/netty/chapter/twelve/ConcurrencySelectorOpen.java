package com.coderli.netty.chapter.twelve;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

/**
 * Selector打开端口数量测试
 * 
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 */
public class ConcurrencySelectorOpen {

	/**
	 * @author lihzh
	 * @alia OneCoder
	 * @throws IOException 
	 * @throws InterruptedException 
	 * @date 2012-8-15 下午1:57:56
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		for (int i = 0; i < 300; i++) {
			Selector selector = Selector.open();
			SocketChannel channel = SocketChannel.open();
			channel.configureBlocking(false);
			channel.connect(new InetSocketAddress("127.0.0.1", 8000));
			System.out.println(i);
			channel.register(selector, SelectionKey.OP_READ);
		}
		Thread.sleep(300000);
	}
}
