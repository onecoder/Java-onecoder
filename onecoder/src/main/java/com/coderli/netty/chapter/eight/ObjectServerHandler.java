package com.coderli.netty.chapter.eight;


import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import com.coderli.netty.chapter.common.model.Command;

/**
 * 对象传递服务端代码
 * 
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 */
public class ObjectServerHandler extends SimpleChannelHandler {
	
	private static int count = 0;

	/**
	 * 当接受到消息的时候触发
	 */
	@Override
	public void messageReceived(ChannelHandlerContext ctx, final MessageEvent e)
			throws Exception {
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				Command command = (Command) e.getMessage();
//				// 打印看看是不是我们刚才传过来的那个
//				count++;
//				System.out.println(command.getActionName() + ": " + count);
//			}
//		}).start();
		Command command = (Command) e.getMessage();
		// 打印看看是不是我们刚才传过来的那个
		count++;
		System.out.println(command.getActionName() + ": " + count);
	}
}
