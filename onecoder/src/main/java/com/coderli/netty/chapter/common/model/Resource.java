package com.coderli.netty.chapter.common.model;

import java.io.Serializable;

/**
 * @author lihzh
 * @date 2012-6-5 上午10:38:56
 */
public class Resource implements Serializable {

	private static final long serialVersionUID = 3792289606595435688L;
	
	private String name;
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
