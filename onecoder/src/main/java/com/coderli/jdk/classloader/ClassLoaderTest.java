package com.coderli.jdk.classloader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author lihzh
 * @date 2012-10-15 下午4:47:05
 */
public class ClassLoaderTest {

	/**
	 *     * 从依赖的Jar包中读取文件, Jar包内的文件是无法用File读取的，只能用Stream的方式读取。     *     * @author
	 * lihzh     * @throws URISyntaxException     * @throws IOException     * @data
	 * 2012-4-11 下午11:07:58     
	 */
	@Test
	public void testGetFileFromJarInClassPath() throws URISyntaxException,
			IOException {
		Enumeration<URL> urls = this.getClass().getClassLoader()
				.getResources("conf/test.properties");
		URL url = this.getClass().getClassLoader()
				.getResource("conf/test.properties");
		Assert.assertTrue(urls.hasMoreElements());
		Assert.assertNotNull(url);
		// 注意两种不同调用方式的路径的区别，在Class中此处如果不以“/” 开头，会被当作相对于当前类所在的包类处理,自然无法找到。
		// 如果以"/"开头，则代表从源码包路径开始。
		// 因为在Class的getResource方法的开头，有一个resolveName方法，处理了传入的路径格式问题。而ClassLoader类里的
		// getResource和getResources均无此处理。使用时候需要用心。
		URL clzURL = this.getClass().getResource("/conf/test.properties");
		URL nullURL = this.getClass().getResource("test/test.properties");
		Assert.assertNotNull(clzURL);
		Assert.assertNull(nullURL);
		URL thisClzURL = this.getClass().getResource("");
		Assert.assertNotNull(thisClzURL);
		// 开始读取文件内容
		InputStream is = this.getClass().getResourceAsStream(
				"/conf/test.properties");
		Properties props = new Properties();
		props.load(is);
		Assert.assertTrue(props.containsKey("test.key"));
		Assert.assertEquals("thisIsValue", props.getProperty("test.key"));
	}
}
