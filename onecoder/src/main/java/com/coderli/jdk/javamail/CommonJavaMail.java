package com.coderli.jdk.javamail;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author lihzh(One Coder)
 * @OneCoder-Blog http://www.coderli.com
 * @date 2012-6-2 下午5:02:33
 */
public class CommonJavaMail {

	public static void main(String args[]) {
		// 构造邮件发送需要的会话（连接信息）
		Properties props = new Properties();
		props.setProperty("mail.smtp.host", "smtp.qq.com");
		props.setProperty("mail.smtp.auth", "true");
		Authenticator myauth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication("mushiqianmeng@vip.qq.com", "");
			}
		};
		Session mailSession = Session.getInstance(props, myauth);

		// 构造邮件发送的主题和内容
		Message  message = new MimeMessage(mailSession);

		try {
			// 正文-文字
			message.setText("Hello world mail");
			// 主题
			message.setSubject("Subject Hello");
			// 发送人
			Address address = new InternetAddress("mushiqianmeng@vip.qq.com");
			message.setFrom(address);
			// 接收人
			message.setRecipient(Message.RecipientType.TO, new InternetAddress("mushiqianmeng@vip.qq.com"));
			Transport.send(message, message.getAllRecipients());

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

}
