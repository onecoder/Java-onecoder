package com.coderli.jdk;

import java.util.Date;

/**
 * @author lihzh(One Coder)
 * @date 2012-6-8 下午9:21:22
 * @blog http://www.coderli.com
 */
public class JDKMain {

	/**
	 */
	public static void main(String[] args) {
		Date date = new Date("Wed Jul 16 00:00:00 CST 2008");
		System.out.println(date);
	}

	/**
	 * 向数组添加元素
	 * 
	 * @param num
	 *            待添加数字
	 * @param index
	 *            待添加位置
	 * @param oriArray
	 *            原始数组
	 */
	private static int[] addToArray(int num, int index, int[] oriArray) {
		int newLength = oriArray.length + 1;
		int newArray[] = new int[newLength];
		for (int i = 0; i < newLength; i++) {
			if (i == index) {
				newArray[i] = num;
			} else if (i < index) {
				newArray[i] = oriArray[i];
			} else {
				newArray[i] = oriArray[i - 1];
			}
		}
		return newArray;
	}
}
