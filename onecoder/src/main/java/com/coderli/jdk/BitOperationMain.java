package com.coderli.jdk;
/**
 * @author lihzh(One Coder)
 * @OneCoder-Blog http://www.coderli.com
 * @date 2012-6-24 下午10:07:18
 */
public class BitOperationMain {

	/**
	 * @param args
	 * @author lihzh(OneCoder)
	 * @date 2012-6-24 下午10:07:18
	 */
	public static void main(String[] args) {
		int a = -7;
		System.out.println(Integer.toBinaryString(a));
		System.out.println(~-7);
	}

}
