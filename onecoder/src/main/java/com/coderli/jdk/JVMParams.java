package com.coderli.jdk;

import java.util.LinkedList;
import java.util.List;

/**
 * JVM参数配置
 * 
 * @author lihzh(OneCoder)
 * @alia OneCoder
 * @Blog http://www.coderli.com
 * @date 2012-7-31 下午9:04:26
 */
public class JVMParams {
	/**
	 * @author lihzh(OneCoder)
	 * @date 2012-7-31 下午9:04:27
	 */
	public static void main(String[] args) {
		 getStackOverFlowError(0);
		 getHeapOutOfMemoryError();
	}

	/**
	 * 通过递归调用，诱发StackOverFlowError
	 * 
	 * @author lihzh
	 * @alia OneCoder
	 */
	private static void getStackOverFlowError(int count) {
		if (count < 1000000) {
			System.out.println("count:" + count);
			getStackOverFlowError(++count);
		}
	}

	/**
	 * 循环创建新对象，构造
	 * <p>
	 * java.lang.OutOfMemoryError: Java heap space
	 * </p>
	 * 异常
	 * 
	 * @author lihzh
	 * @alia OneCoder
	 * @blog http://www.coderli.com
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void getHeapOutOfMemoryError() {
		int count = 0;
		List list = new LinkedList<>();
		for (;;) {
			System.out.println(count++);
			list.add(new Object());
		}
	}
}
