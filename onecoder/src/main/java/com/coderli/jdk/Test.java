package com.coderli.jdk;

class A {
	void f(A a) {
		System.out.println("A");
	}
}

class B extends A {
	void f(B b) {
		System.out.println("B");
	}
}

class C extends B {
	void f(A a) {
		System.out.println("C");
	}
}

public class Test {
	public static void main(String args[]) {
		A a = new C();
		B b = new C();
		a.f(b);
	}
}