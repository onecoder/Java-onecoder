package com.coderli.jdk.concurrent;

import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 测试多线程重入锁
 * 
 * @author OneCoder
 * @date 2013年9月4日 下午10:34:16
 * @website http://www.coderli.com
 */
public class ReentrantLockTest {

	public static void main(String[] args) throws InterruptedException {
		ReentrantLockTest lockTest = new ReentrantLockTest();
		System.out.println("No Lock");
		// 10线程无锁的情况
		lockTest.noLock();
		Thread.sleep(8000L);
		System.out.println("Sychronized lock");
		// 10线程传统锁
		lockTest.synchroLock();
		Thread.sleep(10 * 5000L + 2000L);
		System.out.println("ReentrantLock");
		// 10线程重入锁
		lockTest.reentrantLock();
	}

	/**
	 * 重入锁
	 */
	private void reentrantLock() {
		class ReentrantLockRun implements Runnable {
			ReentrantLock lock = new ReentrantLock();

			@Override
			public void run() {
				lock.lock();
				try {
					System.out.println("Current Thread: "
							+ Thread.currentThread().getName());
					System.out.println(Thread.currentThread().getName()
							+ " Time:" + new Date());
					try {
						Thread.sleep(5000L);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName()
							+ " finish at time: " + new Date());
				} finally {
					lock.unlock();
				}
			}
		}
		ReentrantLockRun reRun = new ReentrantLockRun();
		Thread[] tArray = new Thread[10];
		for (int i = 0; i < 10; i++) {
			Thread t = new Thread(reRun);
			t.setName("Thread-" + i);
			tArray[i] = t;
		}
		for (Thread t : tArray) {
			t.start();
		}

	}

	/**
	 * 传统同步锁，每个线程排队执行
	 */
	private void synchroLock() {
		class SynchLockRun implements Runnable {
			@Override
			public void run() {
				synchronized (this) {
					System.out.println("Current Thread: "
							+ Thread.currentThread().getName());
					System.out.println(Thread.currentThread().getName()
							+ " Time:" + new Date());
					try {
						Thread.sleep(5000L);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName()
							+ " finish at time: " + new Date());
				}
			}
		}
		SynchLockRun sRun = new SynchLockRun();
		Thread[] tArray = new Thread[10];
		for (int i = 0; i < 10; i++) {
			Thread t = new Thread(sRun);
			t.setName("Thread-" + i);
			tArray[i] = t;
		}
		for (Thread t : tArray) {
			t.start();
		}
	}

	// 无锁的情况
	private void noLock() {
		class NoLockRun implements Runnable {
			@Override
			public void run() {
				System.out.println("Current Thread: "
						+ Thread.currentThread().getName());
				System.out.println(Thread.currentThread().getName() + " Time:"
						+ new Date());
				try {
					Thread.sleep(5000L);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(Thread.currentThread().getName()
						+ " finish at time: " + new Date());
			}
		}
		NoLockRun run = new NoLockRun();
		Thread[] tArray = new Thread[10];
		for (int i = 0; i < 10; i++) {
			Thread t = new Thread(run);
			t.setName("Thread-" + i);
			tArray[i] = t;
		}
		for (Thread t : tArray) {
			t.start();
		}
	}
}
