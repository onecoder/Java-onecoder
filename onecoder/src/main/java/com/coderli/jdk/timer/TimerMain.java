package com.coderli.jdk.timer;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author lihzh
 * @alia OneCoder
 * @blog http://www.coderli.com
 */
public class TimerMain {

	/**
	 * JDK Timer类测试类。主要测试在一个Timer周期内，线程未结束时，timer的处理情况。
	 * 
	 * @param args
	 * @author lihzh
	 * @alia OneCoder
	 */
	public static void main(String[] args) {
		TimerTask timerTask = new TimerTask() {
			@Override
			public void run() {
				try {
					System.out.println(Thread.currentThread().getName());
					Thread.sleep(1000 * 5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		Timer timer = new Timer();
		
		timer.scheduleAtFixedRate(timerTask, 0, 1000);
	}
}
