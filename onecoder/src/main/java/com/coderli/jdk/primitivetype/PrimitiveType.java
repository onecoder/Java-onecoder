package com.coderli.jdk.primitivetype;

public class PrimitiveType {
	
	public static void main(String[] args) {
		byte bt = 127;
		int i = 0xf;
		System.out.println(i);
		System.out.println(Integer.toBinaryString(128));
	}

}
