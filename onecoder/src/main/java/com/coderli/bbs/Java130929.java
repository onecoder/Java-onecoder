package com.coderli.bbs;

import java.util.ArrayList;
import java.util.List;

/**
 * 练习题答案： 有以下字符串："nsdssmmanbccd22210crdddddsbcddee", <br>
 * 请用一种方法将字字符串中的同一字符连续出现大于等于2次的提取出放入一个数组中，<br>
 * 结果应为 （["ss","mm","cc","22"]);
 * 
 * @author OneCoder
 * @date 2013年9月29日 下午4:47:24
 * @website http://www.coderli.com
 */
public class Java130929 {

	public static void main(String[] args) {
		String input = "nsdssmmanbccd22210crdddddsbcddee";
		List<String> strList = new ArrayList<String>();
		int pointIndex = 1;
		int length = input.length();
		char[] strChars = input.toCharArray();
		int charCount = 1;
		for (int i = pointIndex; pointIndex < length && i < length; i++) {
			// 比较，如果相等，则计数
			if (strChars[i] == strChars[i - 1]) {
				charCount++;
			} else {
				// 否则指针移动
				if (charCount > 1) {
					String outString = input.substring(pointIndex - 1,
							pointIndex + charCount - 1);
					strList.add(outString);
				}
				pointIndex += charCount;
				charCount = 1;
			}
		}
		// 处理最后几位相同的情况
		if (charCount > 1) {
			strList.add(input.substring(pointIndex - 1, length));
		}
		String[] resultArray = strList.toArray(new String[strList.size()]);
		// 打印输出结果
		for (String string : resultArray) {
			System.out.print(string + " ");
		}
	}
}
