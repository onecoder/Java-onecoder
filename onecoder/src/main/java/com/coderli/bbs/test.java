package com.coderli.bbs;

import java.util.Scanner;

public class test {
	public static void main(String[] args) {
		System.out.println("请输入待比较的数组长度：");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int max = 0;
		for (int i = 0; i < n; i++) {
			System.out.println("请输入一个待比较的整数: ");
			int t = sc.nextInt();
			System.out.println("还需要输入 [" + (n - i - 1) + "] 个。");
			if (i == 0) {
				max = t;
			} else {
				max = max >= t ? max : t;
			}
		}
		System.out.println("最大的数为: " + max);
	}
}