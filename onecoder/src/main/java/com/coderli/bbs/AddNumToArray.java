package com.coderli.bbs;

/**
 * @author lihzh(One Coder)
 * @date 2013年9月4日 下午10:22:10
 * @blog http://www.coderli.com
 */
public class AddNumToArray {

	/**
	 * 在排序好的数组中添加一个数字，将添加的数字插入到数组合适位置
	 * 
	 * @author lihzh
	 * @date 2013年9月4日 下午10:22:14
	 * @blog http://www.coderli.com
	 */
	public static void main(String[] args) {
		int[] oriArray = new int[] { 1, 2, 4, 5 };
		int[] newArray = addToArray(3, 2, oriArray);
		for (int i : newArray) {
			System.out.println(i);
		}
	}

	/**
	 * 向数组添加元素
	 * 
	 * @param num
	 *            待添加数字
	 * @param index
	 *            待添加位置
	 * @param oriArray
	 *            原始数组
	 */
	private static int[] addToArray(int num, int index, int[] oriArray) {
		int newLength = oriArray.length + 1;
		int newArray[] = new int[newLength];
		for (int i = 0; i < newLength; i++) {
			if (i == index) {
				newArray[i] = num;
			} else if (i < index){
				newArray[i] = oriArray[i];
			} else {
				newArray[i] = oriArray[i - 1];
			}
		}
		return newArray;
	}
}
