package com.coderli.bbs;

/**
 * 用三种循环计算1-999每个数字各个位上数字的和，例如： 1 - 001 2 - 002 999 - 027
 * 
 * @author OneCoder
 * @date 2013年9月7日 下午10:11:52
 * @website http://www.coderli.com
 */
public class NumSum {

	// 实际指实现了两重。。
	public static void main(String[] args) {
		// for (int i = 1; i <= 999; i++) {
		// String numStr = String.valueOf(i);
		// int sum = 0;
		// char[] numChars = numStr.toCharArray();
		// for (char c : numChars) {
		// sum += Integer.valueOf(String.valueOf(c));
		// }
		// System.out.println(i + "的和为: " + String.format("%03d", sum));
		// }
		byOneCirCul();
	}

	/**
	 * 真正通过三重循环实现
	 */
	private static void byOneCirCul() {
		for (int i = 1; i <= 999; i++) {
			int unit = i % 10;
			int decade = (i % 100 - unit) / 10;
			int hundred = (i - decade * 10 - unit) / 100;
			int sum = unit + decade + hundred;
			System.out.println(i + "的和为: " + String.format("%03d", sum));
		}
	}

}
