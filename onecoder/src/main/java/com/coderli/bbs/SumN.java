package com.coderli.bbs;

/**
 * 1+2+3+....+n的和 <br>
 * 要求不能使用乘除法、for、while、if、else、switch、case等关键字以及条件
 * 
 * @author OneCoder
 * @date 2013年9月11日 下午9:06:48
 * @website http://www.coderli.com
 */
public class SumN {

	public static void main(String[] args) {
		System.out.println(new SumN().sum(5));
	}

	private int sum(int n) {
		int sum = 0;
		boolean b = (n > 0) && ((sum = n + sum(n - 1)) > 0);
		return sum;
	}
}
