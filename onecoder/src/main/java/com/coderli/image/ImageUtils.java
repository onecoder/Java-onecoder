package com.coderli.image;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

import javax.imageio.ImageIO;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
  
@SuppressWarnings("restriction")
public final class ImageUtils {   
    public ImageUtils() {   
  
    }   
  
      
    /**  
     * 打印文字水印图片  
     *   
     * @param pressText  
     *            --文字  
     * @param targetImg --  
     *            目标图片  
     * @param fontName --  
     *            字体名  
     * @param fontStyle --  
     *            字体样式  
     * @param color --  
     *            字体颜色  
     * @param fontSize --  
     *            字体大小  
     * @param x --  
     *            偏移量  
     * @param y  
     */  
  
	public static void pressText(String pressText, String targetImg,   
            String fontName, int fontStyle, Color color, int fontSize, int x,   
            int y) {   
        try {   
            File _file = new File(targetImg);   
            Image src = ImageIO.read(_file);   
            int width = src.getWidth(null);   
            int height = src.getHeight(null);   
            BufferedImage image = new BufferedImage(width, height,   
                    BufferedImage.TYPE_INT_RGB);   
            Graphics g = image.createGraphics();   
            g.drawImage(src, 0, 0, width, height, null);   
            g.setColor(color);   
            g.setFont(new Font(fontName, fontStyle, fontSize));   
  
            g.drawString(pressText, width - fontSize - x, height - fontSize   
                    / 2 - y);   
            g.dispose();   
            FileOutputStream out = new FileOutputStream(targetImg);   
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);   
            encoder.encode(image);   
            out.close();   
        } catch (Exception e) {   
            System.out.println(e);   
        }   
    }   
  
    public static void main(String[] args) {   
        pressText("bbs.coderli.com", "f:/1.jpg", "Times New Romas", Font.PLAIN, Color.BLUE, 22,  150, 20);
    }   
}   