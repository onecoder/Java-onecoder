package com.coderli.algorithm.philosopher;

/**
 * 哲学家问题代码
 * 
 * <pre>
 * http://zh.wikipedia.org/wiki/%E5%93%B2%E5%AD%A6%E5%AE%B6%E5%B0%B1%E9%A4%90
 * %E9%97%AE%E9%A2%98
 * 
 * @author OneCoder
 * @date 2013年9月8日 下午10:15:42
 * @website http://www.coderli.com
 */
public class PhilosopherProblem {

	public static void main(String[] args) {
		char  x = 'X';
		final int  i = 65536;
		System.out.print(true ? x : 0);
		System.out.println(false ? i : x);
	}
}
